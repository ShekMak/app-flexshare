#!/usr/clearos/sandbox/usr/bin/php
<?php

/**
 * Flexshare script.
 *
 * @category   apps
 * @package    flexshare
 * @subpackage scripts
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2013-2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/flexshare/
 */

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');
clearos_load_language('flexshare');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\flexshare\Flexshare as Flexshare;

clearos_load_library('flexshare/Flexshare');

///////////////////////////////////////////////////////////////////////////////
// O P T I O N S
/////////////////////////////p//////////////////////////////////////////////////

$short_options = '';
$short_options .= 'n:'; // Share
$short_options .= 'a:'; // Action
$short_options .= 'g:'; // Group
$short_options .= 'd:'; // Directory
$short_options .= 'j'; // Output format in json
$short_options .= 'h'; // Help

$long_options = ['description:', 'delete_dir'];

$help_options  = '';
$help_options .= "  -n: Flexshare name\n";
$help_options .= "  -a: Action (activate, deactivate, create, delete, list)\n";
$help_options .= "  -d: Directory\n";
$help_options .= "  -g: Group\n";
$help_options .= "  -j: Output format in json\n";
$help_options .= "  --description: Description\n";
$help_options .= "  --delete_dir: Flag to delete Flexshare folder on delete\n";
$help_options .= "\n";
$help_options .= "  -h: Help\n";

$options = getopt($short_options, $long_options);

$help = isset($options['h']) ? TRUE : FALSE;
$name = isset($options['n']) ? $options['n'] : '';
$action = isset($options['a']) ? $options['a'] : '';
$directory = isset($options['d']) ? $options['d'] : '';
$group = isset($options['g']) ? $options['g'] : '';
$description = isset($options['description']) ? $options['description'] : '';
$delete_dir = isset($options['delete_dir']);
$json = isset($options['j']) ? TRUE : FALSE;

///////////////////////////////////////////////////////////////////////////////
// M A I N
///////////////////////////////////////////////////////////////////////////////

// Basic usage stuff
//------------------

if ($help) {
    echo "usage: " . $argv[0] . " [options]\n";
    echo $help_options;
    exit(0);
}

// Handle command line options
//----------------------------

$flexshare = new Flexshare();

$requires_name = ['activate', 'deactivate', 'delete'];

if (($action != 'list')) {
    while (empty($name)) {
        echo 'Flexshare name (e.g. myshare): ';
        $name = trim(fgets(STDIN));
    }
}

while (($action != 'activate') && ($action != 'deactivate') && ($action != 'create') && ($action != 'delete') && ($action != 'list')) {
    echo 'Action (activate, deactivate, create, delete or list): ';
    $action = trim(fgets(STDIN));
}

try {
    if (in_array($action, $requires_name)) {
        if (!$flexshare->exists($name))
            status(1, lang('flexshare_share_not_found'));
        if ($action === 'activate') {
            $flexshare->activate($name);
            status(0, lang('base_item_was_added'));
        } else if ($action === 'deactivate') {
            status(1, "TODO");
        } else if ($action === 'delete') {
            $flexshare->delete_share($name, $delete_dir);
            status(0, lang('base_item_was_deleted'));
        }
    } else if ($action === 'create') {
        $share = strtolower($name);
        if (empty($directory) || ($directory == Flexshare::SHARE_PATH))
            $directory = Flexshare::SHARE_PATH . '/' . $share;
        try {
            $flexshare->add_share(
                $name,
                $description,
                $group,
                $directory
            );
            status(0, lang('base_item_was_added'));
        } catch (Exception $e) {
            status(1, $e->getMessage());
        }
    } else if ($action === 'list') {
        $list = $flexshare->get_share_summary(false);
        status(0, $list);
    } else {
        status(1, "Invalid/unknown action");
    }
} catch (\Exeption $e) {
    status (100, $e->getMessage()); 
}

/**
 * .
 *
 * @param string $code     status code
 * @param string $data     data
 *
 * @return void
 */

function status($code, $data)
{
    global $json;

    if ($json) {
        $info = array (
            'code' => $code,
            'data' => $data
        );
        echo json_encode($info) . "\n"; 
        exit($code);
    } else {
        if (is_array($data)) {
            print_r($data);
        } else {
            echo $data . "\n";
        }
        exit($code);
    }
}
